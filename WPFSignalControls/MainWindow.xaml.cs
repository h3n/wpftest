﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFSignalControls
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Signal OdoSignal { get; set; } = new Signal("Odo");
        public Signal FooSignal { get; set; } = new Signal("Foo");
        public Signal BarSignal { get; set; } = new Signal("Bar");

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }
    }
}
