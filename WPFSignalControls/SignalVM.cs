﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFSignalControls
{
    public class SignalVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private Signal signal;

        public string SignalName => signal?.Name ?? "Unknown Signal";
        

        public int SignalValue
        {
            get
            {
                return signal?.Value ?? -1;
            }
            set
            {
                if (signal != null)
                    signal.Value = value;
            }
        }
        public SignalVM(Signal signal)
        {
            this.signal = signal;
            signal.Changed += (s, v) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SignalValue"));
        }
    }
}
