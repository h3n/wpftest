﻿using System.Windows;
using System.Windows.Controls;

namespace WPFSignalControls
{
    /// <summary>
    /// Interaktionslogik für SignalControl.xaml
    /// </summary>
    public class SignalControl : UserControl
    {
        public static readonly DependencyProperty SignalProperty =
            DependencyProperty.Register("Signal", typeof(Signal), typeof(SignalControl), new PropertyMetadata(null, new PropertyChangedCallback(OnSignalChanged)));

        private static void OnSignalChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                ((SignalControl)d).DataContext = new SignalVM((Signal)e.NewValue);
            }
        }

        public Signal Signal
        {
            get
            {
                return (Signal)GetValue(SignalProperty);
            }
            set
            {
                SetValue(SignalProperty, value);
            }
        }

        public SignalControl()
        {
        }
    }
}
