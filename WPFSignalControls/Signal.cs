﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFSignalControls
{
    public partial class Signal
    {
        private static Random random = new Random();
        public delegate void ChangedHandler(Signal sender, int newValue);
        public string Name { get; set; }
        private int value;
        public int Value
        {
            get { return value; }
            set
            {
                this.value = value;
                Changed?.Invoke(this, value);
            }
        }
        public event ChangedHandler Changed;

        public Signal(string name)
        {
            Name = name;
            Value = 1;
            new System.Threading.Thread(() =>
            {
                while (Value > 0)
                {
                    Value = random.Next(100);
                    System.Threading.Thread.Sleep(5000);
                }
            }).Start();
        }

    }
}
